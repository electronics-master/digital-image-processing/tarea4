# Homework 4

This repository includes the code and information about Homework 4 for
the MP6123-Digital Signal Processing course in the Electronics Master
of the Costa Rica Institute of Technology.

## Analysis

The results on which this anaylisis is based can be seen in the
following graph:

<img src="./result.svg">

The same input image was used for all cases, for each kernel size the
experiment was ran 100 times and this was measured using C++'s chrono
library.

The separable filter exposes a `O(n)` linear behaviour as expected by
the theory.

For the the non-separable filter it starts with a much more steep
increasing behaviour `O(n^2)`. before reaching a ceiling value before
a kernel size of `10x10`. After which it keeps a mostly linear
behaviour, the reason for this is due to the implementation of the
filter, where after filter values of `11x11` it uses a dft-based
convolution[1].

For the frequency filtering this keeps a mostly constant processing
time. In the implemented algorithm the optimal size for the
convolution is obtained based on the image size and the kernel size,
if the image is much larger than the kernel the changes in the kernel
size will have a much smaller impact than the actual image size. The
`O(nlog(n))` behaviour will most likely still keep for the image size
but since this experiment is studying the impact of the kernel size
this behaviour is not seen. The small impact of the kernel size can
actually be seen as image size increases.

### Recommended Mask Size

For kernel sizes below 30 pixels the recommended filter is a separable
filters. For kernels larger than these the non-seperabake filter is
recommended.

Take note that all of these values are using the OpenCV library, which
means that for values larger than `11x11` the non-separable filter is
actually performing a frequency filtering. Which makes the frequency
filtering the optimal algorithm and this might change for other
implementations.

## Running the Program

### Requirements

```bash
sudo apt install libgnuplot-iostream-dev libboost-program-options-dev libopencv-dev
```

### Building

```bash
make
```

### Running


#### All options
```bash
./convolution_test --input image.jpg
```

The input is the only required argument, all other can be configured
by the following options.

```
Input is required
Allowed Options:
  --help                 Produce help message
  --input arg            Input image to process
  --iters arg (=20)      Iterations per each kernel size
  --min-kernel arg (=3)  Minimun kernel size
  --max-kernel arg (=31) Maximum kernel size
```

## References

[1] https://docs.opencv.org/4.4.0/d4/d86/group__imgproc__filter.html#ga27c049795ce870216ddfb366086b5a04
