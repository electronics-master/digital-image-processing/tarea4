// Copyright 2020 Emmanuel Madrigal <emma.madri97@gmail.com>
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// Other Libraries
#include <boost/program_options.hpp>
#include <gnuplot-iostream.h>
#include <opencv2/opencv.hpp>

// Standard Library
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>

#define ANCHOR cv::Point(-1, -1)
#define BORDER_TYPE cv::BORDER_CONSTANT
#define DELTA 0
#define IMAGE_DEPTH CV_64F
#define MICROSECS_TO_MILLISECS 1000.0

namespace po = boost::program_options;

static cv::Mat apply_2d_filter(const cv::Mat image, const cv::Mat kernel);
static cv::Mat apply_dft_2d_filter(const cv::Mat image, const cv::Mat kernel,
                                   const int orig_width, const int orig_height);
static cv::Mat apply_separable_2d_filter(const cv::Mat image,
        const cv::Mat kernel_x, const cv::Mat kernel_y);
static cv::Mat circular_shift(const cv::Mat input_image,
                              const unsigned int vertical_shift, const unsigned int horizontal_shift);
static cv::Mat dft_to_image(const cv::Mat freq_image, const int orig_width,
                            const int orig_height);
static cv::Mat get_filter(const int kernel_size, const bool separable=false);
static void get_optimal_size(const cv::Mat image, const cv::Mat kernel, int &m,
                             int &n);
static cv::Mat image_to_dft(const cv::Mat image);
static int process(const std::string input, const int iters, const int min_kernel, const int max_kernel);

cv::Mat circular_shift(const cv::Mat input_image,
                       const unsigned int vertical_shift, const unsigned int horizontal_shift) {
    // Applies a circular shift towards the upper left
    cv::Mat shifted = cv::Mat::zeros(input_image.size(), input_image.type());

    // abb -> ddc
    // cdd -> ddc
    // cdd -> bba

    // TODO check that vertical_shift and horizontal_shift have valid values

    int vertical_border = input_image.rows - vertical_shift;
    int horizontal_border = input_image.cols - horizontal_shift;

    // A
    input_image(cv::Rect(0, 0,
                         horizontal_shift, vertical_shift)).copyTo(
                             shifted(cv::Rect(horizontal_border, vertical_border,
                                     horizontal_shift, vertical_shift)));

    // B
    input_image(cv::Rect(horizontal_shift, 0,
                         horizontal_border, vertical_shift)).copyTo(
                             shifted(cv::Rect(0, vertical_border,
                                     horizontal_border, vertical_shift)));

    // C
    input_image(cv::Rect(0, vertical_shift,
                         horizontal_shift, vertical_border)).copyTo(
                             shifted(cv::Rect(horizontal_border, 0,
                                     horizontal_shift, vertical_border)));

    // D
    input_image(cv::Rect(horizontal_shift, vertical_shift,
                         horizontal_border, vertical_border)).copyTo(
                             shifted(cv::Rect(0, 0,
                                     horizontal_border, vertical_border)));

    return shifted;
}

cv::Mat get_filter(const int kernel_size, const bool separable) {
    cv::Mat filter;
    cv::Mat transposed_filter;
    double sigma;

    sigma = (kernel_size + 2) / 6;

    filter = cv::getGaussianKernel(kernel_size, sigma);
    if (separable) {
        return filter;
    } else {
        cv::transpose(filter, transposed_filter);
        return filter * transposed_filter;
    }
}

cv::Mat apply_2d_filter(const cv::Mat image, const cv::Mat kernel) {
    cv::Mat output;

    cv::filter2D(image, output, IMAGE_DEPTH, kernel, ANCHOR, DELTA, BORDER_TYPE);

    return output;
}

cv::Mat apply_separable_2d_filter(const cv::Mat image, const cv::Mat kernel_x,
                                  const cv::Mat kernel_y) {
    cv::Mat output;

    cv::sepFilter2D(image, output, IMAGE_DEPTH, kernel_x, kernel_y, ANCHOR, DELTA,
                    BORDER_TYPE);

    return output;
}

void get_optimal_size(const cv::Mat image, const cv::Mat kernel, int &m,
                      int &n) {
    m = cv::getOptimalDFTSize( image.rows + kernel.rows - 1 );
    n = cv::getOptimalDFTSize( image.cols + kernel.cols - 1 );
}

cv::Mat pad_image(const cv::Mat image, const int m, const int n) {
    cv::Mat padded_image;

    cv::copyMakeBorder(image, padded_image, 0, m - image.rows, 0, n - image.cols,
                       BORDER_TYPE, cv::Scalar(0));

    return padded_image;

}

cv::Mat image_to_dft(const cv::Mat image) {
    cv::Mat float_image;
    cv::Mat freq_image;

    image.convertTo(float_image, CV_64F);

    cv::dft(float_image, freq_image, cv::DFT_COMPLEX_OUTPUT);

    return freq_image;
}

cv::Mat dft_to_image(const cv::Mat freq_image, const int orig_width,
                     const int orig_height) {
    cv::Mat space_image;

    cv::dft(freq_image, space_image,
            cv::DFT_INVERSE | cv::DFT_REAL_OUTPUT | cv::DFT_SCALE);

    return space_image(cv::Rect(0, 0, orig_height, orig_width));
}

cv::Mat apply_dft_2d_filter(const cv::Mat image, const cv::Mat kernel,
                            const int orig_width,
                            const int orig_height) {
    cv::Mat filtered;

    cv::mulSpectrums(image, kernel, filtered, 0);

    return dft_to_image(filtered, orig_width, orig_height);
}

int process(const std::string input, const int iters, const int min_kernel, const int max_kernel) {
    const cv::Vec3b color;
    cv::Mat filter;
    cv::Mat filtered_non_sep, filtered_sep, filtered_freq;
    cv::Mat input_image;
    int m, n;
    std::vector<std::pair<int, double>> filter_time, sep_filter_time, freq_filter_time;

    input_image = cv::imread(input, cv::IMREAD_GRAYSCALE);

    if(!input_image.data) {
        std::cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

    for (int i = 0; i < 10; i++ ) {
        for (int j = 0; j < 10; j++ ) {
            input_image.at<uchar>(i,j) = i + j;
        }
    }

    for (int i = min_kernel; i <= max_kernel; i ++) {
        // Process Non-Separable Filter
        filter = get_filter(i, false);
        auto begin = std::chrono::steady_clock::now();
        for (int j = 0; j < iters; j++) {
            filtered_non_sep = apply_2d_filter(input_image, filter);
        }
        auto end = std::chrono::steady_clock::now();
        double time = std::chrono::duration_cast<std::chrono::microseconds>
                    (end - begin).count() / iters / MICROSECS_TO_MILLISECS;
        filter_time.emplace_back(i, time);

        // Process Separable Filter
        filter = get_filter(i, true);
        begin = std::chrono::steady_clock::now();
        for (int j = 0; j < iters; j++) {
            filtered_sep = apply_separable_2d_filter(input_image, filter, filter);
        }
        end = std::chrono::steady_clock::now();
        time = std::chrono::duration_cast<std::chrono::microseconds>
               (end - begin).count() / iters / MICROSECS_TO_MILLISECS;
        sep_filter_time.emplace_back(i, time);

        // Process Filtering in Frequency
        filter = get_filter(i, false);
        get_optimal_size(input_image, filter, m, n);
        cv::Mat padded_filter = pad_image(filter, m, n);
        cv::Mat rolled_filter = circular_shift(padded_filter, i/2, i/2);
        cv::Mat dft_filter = image_to_dft(rolled_filter);
        begin = std::chrono::steady_clock::now();
        for (int j = 0; j < iters; j++) {
            cv::Mat padded_image = pad_image(input_image, m, n);
            cv::Mat dft_image = image_to_dft(padded_image);
            filtered_freq = apply_dft_2d_filter(dft_image, dft_filter,
                                                input_image.rows, input_image.cols);

        }
        end = std::chrono::steady_clock::now();
        time = std::chrono::duration_cast<std::chrono::microseconds>
               (end - begin).count() / iters / MICROSECS_TO_MILLISECS;
        freq_filter_time.emplace_back(i, time);
    }

    Gnuplot gp;
    gp << "set ylabel 'Processing Time [ms]'\n";
    gp << "set xlabel 'Kernel Side [pixels]'\n";
    gp << "plot '-' with lines title 'Non-Separable', ";
    gp << "'-' with lines title 'Separable', ";
    gp << "'-' with lines title 'Frequency-Filtering'\n";
    gp.send1d(filter_time);
    gp.send1d(sep_filter_time);
    gp.send1d(freq_filter_time);

    return 0;
}

int main(int ac, char* av[]) {
    std::string input;
    int min, max;

    try {
        po::options_description desc("Allowed Options");
        desc.add_options()
        ("help", "Produce help message")
        ("input", po::value<std::string>(), "Input image to process")
        ("iters", po::value<int>()->default_value(20), "Iterations per each kernel size")
        ("min-kernel", po::value<int>()->default_value(3), "Minimun kernel size")
        ("max-kernel", po::value<int>()->default_value(31), "Maximum kernel size");

        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, desc), vm);
        po::notify(vm);

        if(vm.count("help") != 0u) {
            std::cout << desc << "\n";
            return 0;
        }

        if(vm.count("input") != 0u) {
            input = vm["input"].as<std::string>();
        } else {
            std::cout << "Input is required" << "\n";
            std::cout << desc << "\n";
            return 1;
        }

	min = vm["min-kernel"].as<int>();
	max = vm["max-kernel"].as<int>();

	if (min <= 1) {
	  std::cerr << "Minimum kernel size should be larger than 1" << "\n";
	  return -1;
	}

	if (max <= 1) {
	  std::cerr << "Maximum kernel size should be larger than 1" << "\n";
	  return -1;
	}
	
	if (min > max) {
	  std::swap(min, max);
	  std::cout << "Min should be smaller than max, swapping values" << "\n";
	}

        return process(input, vm["iters"].as<int>(), min, max);

    } catch(std::exception& e) {
        std::cerr << "error: " << e.what() << "\n";
        return 1;
    } catch(...) {
        std::cerr << "Exception of unknown type!\n";
    }

    return 0;

}
